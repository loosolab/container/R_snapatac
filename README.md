# scATAC SnapATAC/Episcanpy

A container to use Episcanpy within snaptools and snapatac in a single enviroment. Build from the r-data.analysis base container 

### Obtaining the container

```
docker pull docker.gitlab.gwdg.de/loosolab/container/r_snapatac:latest

```

### Running the container

```
docker run -d -p 172.16.30.9:8787:8787 -p 172.16.30.9:8888:8888 -e USERID=$UID -v /path/to/workspace:/home/rstudio/ docker.gitlab.gwdg.de/loosolab/container/r_snapatac:latest
```

### Available packages

All packages from the [rocker/tidyverse](https://hub.docker.com/r/rocker/tidyverse) plus

- `multipanelfigure`
- `ComplexHeatmap`
- `tinytex`
- `rmarkdown`
- `plotly`
- `rprojroot`
- `Matrix.utils`
- `rstudio/gt`
- `i2dash`
- `Snaptools`
- `SnapATAC`
- `MACS2`
- `rpy2`
- `EpiScanpy`
- `Jupyter-Notebook`

### Building the container

```
docker build --squash -t snapatac-r-base https://gitlab.gwdg.de/loosolab/container/r_snapatac.git
```

### Accessing the Container 

- `docker exec -it 'container-id' bash`
- `rstudio accessable via browser (PORT 8787)`
- `jupyter notebook accessable via browser (PORT 8888) (Token=b06eebe56fbf0c5551915eba608da61ac4d8d998232c879d)`


