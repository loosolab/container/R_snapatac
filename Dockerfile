FROM rocker/tidyverse:latest
LABEL maintainer="Jan Detleffsen <jan.detleffsen@mpi-bn.mpg.de>"

# ADD VARIABLE FOR WILSON HANDLING
#ARG ADD_WILSON=FALSE
ARG ADD_SNAPATAC=TRUE
#Hack to read custom setupR
COPY . /app

RUN \
    # Install additional tools
    ## Assure up-to-date package lists
    apt-get update && \
    ## Assure up-to-date environment
    apt-get dist-upgrade --assume-yes && \
    apt-get install --assume-yes --no-install-recommends \
      ## Fetch additional libraries
      libssh2-1-dev libgit2-dev \
      ## Fetch the additional tools
      git-lfs gnupg ssh-client \
      # Install libraries needed for compilation of the autonomics toolkit
      ## Dependencies of rgl
      libx11-dev libglu1-mesa-dev \
      ## Dependencies of roxygen2
      libxml2-dev zlib1g-dev \
      ## Dependencies of multipanelfigure (etc.)
      libmagick++-dev \
      ##Dependencies snaptools
      python3-dev \
      python3-pip \
      libhdf5-dev \
      ##Dependencies snapatac
      tk8.6-dev \ 
      libglpk-dev &&\
    # Clean the cache(s)
    apt-get clean && \
    rm -r /var/lib/apt/lists/* 
    # Install R infrastructure
  RUN \
  pip install jupyter &&\
  pip install snaptools
  #Install episcanpy dependencies
  RUN \
  pip install -r /app/requirements.txt &&\
  pip install episcanpy
  RUN \
  pip install MACS2

  RUN \
    sudo -u rstudio ADD_SNAPATAC=${ADD_SNAPATAC} Rscript -e 'source("/app/setup.R")' 
  RUN \
    sudo -u rstudio Rscript -e 'source("https://gitlab.gwdg.de/loosolab/container/r-data.analysis/raw/master/tinytex.R")'
  RUN \
    # Set the time zone
    sh -c "echo 'Europe/Berlin' > /etc/timezone" && \
    dpkg-reconfigure -f noninteractive tzdata && \
    # Clean /tmp (downloaded packages)
    rm -r /tmp/* && \
    # Generate an ssh key
    sudo -u rstudio ssh-keygen -t ed25519 -N "" -f /home/rstudio/.ssh/id_ed25519

ENV \
    ROOT=TRUE \
    DISABLE_AUTH=TRUE \
    LD_LIBRARY_PATH=/usr/local/lib/R/lib/:${LD_LIBRARY_PATH}

CMD ["/app/start.sh"]